package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/charmbracelet/log"
	"gopkg.in/yaml.v3"
)

var (
	// flags
	flagInputDir  string
	flagOutputDir string
	flagVerbose   bool

	// csvHeader represents the header (first row) that contains the names
	// of the fields written to the CSV by order.
	csvHeader = []string{
		"uuid",
		"created_at",
		"updated_at",
		"id",
		"created_date",
		"published_date",
		"description",
		"title",
		"component_name",
		"solution",
		"not_impacted",
		"cvss_v2",
		"cvss_v3",
		"affected_range",
		"identifiers",
		"fixed_versions",
		"urls",
		"links",
	}

	// TODO: Re-enable `pypi`. It is excluded due to invalid utf-8 byte in a description and/or
	// solution string which causes an issue when ingested by Postgres.
	// pkgManagers = []string{"conan", "gem", "go", "maven", "npm", "nuget", "packagist", "pypi"}
	pkgManagers = []string{"conan", "gem", "go", "maven", "npm", "nuget", "packagist"}
)

func main() {
	flag.StringVar(&flagInputDir, "input-dir", "", "path to the gitlab advisory database")
	flag.StringVar(&flagOutputDir, "output-dir", ".", "path to output directory")
	flag.BoolVar(&flagVerbose, "v", false, "enables verbose output")
	flag.Parse()

	if len(flagInputDir) == 0 {
		log.Fatal("Path to gitlab advisory database not given")
	}

	if flagVerbose {
		log.SetLevel(log.DebugLevel)
	}

	inputDir, err := filepath.Abs(flagInputDir)
	if err != nil {
		log.Fatal("Could not resolve absolute path to gitlab advisory database", "err", err)
	}

	outputDir, err := filepath.Abs(flagOutputDir)
	if err != nil {
		log.Fatal("Could not resolve absolute path to default output directory", "err", err)
	}

	result, err := NewResult(outputDir)
	if err != nil {
		log.Fatal("Could not initialize result set", "err", err)
	}
	for _, pkgManager := range pkgManagers {
		searchDir := filepath.Join(inputDir, pkgManager)
		out, err := findAdvisories(searchDir)
		if err != nil {
			log.Fatal("Unrecoverable error encountered")
		}
		exportPath := filepath.Join(outputDir, pkgManager+".csv")
		err = writeCSVFile(exportPath, pkgManager, out)
		if err != nil {
			log.Error("Writing advisories", "pkg_manager", pkgManager, "err", err)
			continue
		}
		result.Exports = append(result.Exports, Export{
			PackageManager: pkgManager,
			Path:           exportPath,
			TempTableName:  "tmp_" + pkgManager + "_advisories",
		})
	}
	err = result.GenScript()
	if err != nil {
		log.Fatal("Import script not generated", "err", err)
	}
}

func findAdvisories(searchDir string) ([]*Advisory, error) {
	out := make([]*Advisory, 0, 4096)
	err := filepath.WalkDir(searchDir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			log.Error("Walking directory", "path", path, "err", err)
			return err
		}

		if filepath.Ext(path) == ".yml" {
			log.Debug("Found eligible file", "path", path)

			f, err := os.Open(path)
			if err != nil {
				return fmt.Errorf("opening file %s: %w", path, err)
			}

			var advisory Advisory
			err = yaml.NewDecoder(f).Decode(&advisory)
			if err != nil {
				return fmt.Errorf("decoding %s into advisory struct: %w", path, err)
			}

			err = f.Close()
			if err != nil {
				return fmt.Errorf("closing %s file handle: %w", path, err)
			}
			out = append(out, &advisory)
		}
		return nil
	})
	return out, err
}

func writeCSVFile(exportPath, pkgManager string, advisories []*Advisory) error {
	if len(advisories) == 0 {
		log.Infof("Skipping %s - no advisories found", pkgManager)
		return nil
	}

	f, err := os.Create(exportPath)
	if err != nil {
		return err
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Warnf("Closing %s: %v", f.Name(), err)
		} else {
			log.Debugf("Closed %s", f.Name())
		}
	}()

	w := csv.NewWriter(f)
	w.Write(csvHeader)
	for idx := range advisories {
		advisory := advisories[idx]
		w.Write(advisory.csvRecord())
		if idx%500 == 0 {
			w.Flush()
			log.Info("Writing advisories to export file", "file", f.Name(), "curr", idx+1, "total", len(advisories)+1)
		}
		if w.Error() != nil {
			return fmt.Errorf("writing to file: %w", w.Error())
		}
	}

	w.Flush()
	if w.Error() != nil {
		return fmt.Errorf("writing to file: %w", w.Error())
	}
	log.Info("Finished writing advisores to export file", "file", f.Name(), "curr", len(advisories)+1, "total", len(advisories)+1)

	return nil
}
