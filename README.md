# glad-to-csv

This is a small tool that converts the advisories in the gemnasium-db, a.k.a the GitLab Advisory Database, into
CSV exports that can be copied into the [vulnerability_advisories](https://gitlab.com/gitlab-org/gitlab/-/blob/3a449a1f/ee/spec/factories/vulnerabilities/advisories.rb).
table. For ease of use, it also generates a script that can be executed by Postgres to ingest the CSV exports.

## Usage

```shell
# From the root of the repository execute the following
$ go build -o converter .

$ ./converter -h
Usage of ./converter:
  -input-dir string
        path to the gitlab advisory database
  -output-dir string
        path to output directory (default ".")
  -v    enables verbose output

# Example usage
$ ./converter -input-dir ~/code/gemnasium-db -output-dir ~/code/gdk/gitlab

$ gdk psql -f ~/code/gdk/gitlab/import_glad_advisories.sql
```

## Status

This is an experimental tool used only for prototyping! It's been tested on [gitlab-org/gitlab @ 3a449a1f](https://gitlab.com/gitlab-org/gitlab/-/blob/3a449a1f/ee/spec/factories/vulnerabilities/advisories.rb).

## Known issues

- If a string field has an invalid UTF-8 sequence it will not be handled by the converter, and as a result, Postgres will error when
running the generated script. The `pypi` directory in the advisory database has this issue, and is currently _not_ exported.
