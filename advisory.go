package main

import (
	"fmt"
	"strings"
	"sync/atomic"
	"time"
)

// advisorySeqID mimics a postgres sequence id and is used for the required advisory id column
var advisorySeqID uint64

// Advisory is a security advisory published for a package.
type Advisory struct {
	// Identifier is CVE id (preferred) or any public identifier.
	Identifier string `yaml:"identifier,omitempty" json:"identifier,omitempty"`

	// Identifier is CVE id (preferred) or any public identifiers.
	Identifiers []string `yaml:"identifiers,omitempty" json:"identifiers,omitempty"`

	// Title is a short description of the security flaw.
	Title string `yaml:"title" json:"title"`

	// Description is a long description of the security flaw and the possible risks.
	Description string `yaml:"description" json:"description"`

	// DisclosureDate is the date on which the advisory was made public, in ISO-8601 format.
	DisclosureDate string `yaml:"date,omitempty" json:"date,omitempty"`

	// AffectedRange is the range of affected versions. Machine-readable syntax used by the package manager.
	AffectedRange string `yaml:"affected_range" json:"affected_range"`

	// CVSSV2 is the Common Vulnerability Scoring System v2 compressed textual
	// representation of the characteristics of a vulnerability.
	CVSSV2 string `yaml:"cvss_v2,omitempty" json:"cvss_v2,omitempty"`

	// CVSSV3 is the Common Vulnerability Scoring System v3.x compressed textual
	// representation of the characteristics of a vulnerability.
	CVSSV3 string `yaml:"cvss_v3,omitempty" json:"cvss_v3,omitempty"`

	// FixedVersions are the versions fixing the vulnerability. The order is not relevant.
	FixedVersions []string `yaml:"fixed_versions,omitempty" json:"fixed_versions,omitempty"`

	// ImpactedVersions is the range of affected versions. Human-readable version for display.
	ImpactedVersions string `yaml:"affected_versions,omitempty" json:"affected_versions,omitempty"`

	// NotImpacted describes the environments not affected by the vulnerability.
	NotImpacted string `yaml:"not_impacted,omitempty" json:"not_impacted,omitempty"`

	// Solution tells how to remediate the vulnerability.
	Solution string `yaml:"solution,omitempty" json:"solution,omitempty"`

	// Credit gives the names of the people who reported the vulnerability or helped fixing it.
	Credit string `yaml:"credit,omitempty" json:"credit,omitempty"`

	// Links are the URLs of: detailed advisory, documented exploit, vulnerable source code, etc.
	Links []string `yaml:"urls" json:"urls"`

	// Package is the affected package.
	Package Package `yaml:"package_slug" json:"package_slug"`

	// UUID is the identifier in Gemnasium DB. It's no longer used.
	UUID string `yaml:"uuid,omitempty" json:"uuid,omitempty"`

	// URL is the web URL of the advisory. This field is calculated when loading the advisory.
	// It is encoded to JSON for internal communication b/w the scanner and the convert package.
	URL string `yaml:"-" json:"url,omitempty"`

	// Version meta-information
	Versions []Version `yaml:"versions,omitempty" json:"versions,omitempty"`
}

// identifiersString converts the Identifiers field (string slice) into a compatible string that
// can be parsed by Postgres.
func (a Advisory) identifiersString() string {
	if len(a.Identifiers) == 0 {
		return "{}"
	}
	ids := make([]string, 0, len(a.Identifiers))
	for idx := range a.Identifiers {
		ids = append(ids, fmt.Sprintf("%q", a.Identifiers[idx]))
	}
	return "{" + strings.Join(ids, ",") + "}"
}

// linksString converts the Links field (string slice) into a compatible string that
// can be parsed by Postgres.
func (a Advisory) linksString() string {
	if len(a.Links) == 0 {
		return "{}"
	}
	links := make([]string, 0, len(a.Links))
	for idx := range a.Links {
		links = append(links, fmt.Sprintf("%q", a.Links[idx]))
	}
	return "{" + strings.Join(links, ",") + "}"
}

// fixedVersions converts the FixedVersions field (string slice) into a compatible string that
// can be parsed by Postgres.
func (a Advisory) fixedVersionsString() string {
	if len(a.FixedVersions) == 0 {
		return "{}"
	}
	fixedVersions := make([]string, 0, len(a.FixedVersions))
	for idx := range a.FixedVersions {
		fixedVersions = append(fixedVersions, fmt.Sprintf("%q", a.FixedVersions[idx]))
	}
	return "{" + strings.Join(fixedVersions, ",") + "}"
}

func (a Advisory) csvRecord() []string {
	now := time.Now().Format(time.RubyDate)
	atomic.AddUint64(&advisorySeqID, 1)

	// Mimic the constraints set in the Postgres schema for `vulnerability_advisories`.
	if len(a.Title) > 2048 {
		a.Title = a.Title[:32]
	}
	if len(a.Package.Name) > 2048 {
		a.Package.Name = a.Package.Name[:2048]
	}
	if len(a.Solution) > 2048 {
		a.Solution = a.Solution[:2048]
	}
	if len(a.NotImpacted) > 2048 {
		a.NotImpacted = a.NotImpacted[:2048]
	}
	if len(a.Description) > 2048 {
		a.Description = a.Description[:2048]
	}
	if len(a.AffectedRange) > 32 {
		a.AffectedRange = a.AffectedRange[:32]
	}

	return []string{
		// uuid
		a.UUID,
		// created_at
		now,
		// updated_at
		now,
		// id
		fmt.Sprintf("%d", advisorySeqID),
		// created_date
		a.DisclosureDate,
		// published_date
		a.DisclosureDate,
		// description
		a.Description,
		// title
		a.Title,
		// component_name - known to cause collisions
		a.Package.Name,
		// solution
		a.Solution,
		// not_impacted
		a.NotImpacted,
		// cvss_v2
		a.CVSSV2,
		// cvss_v3
		a.CVSSV3,
		// affected_range
		a.AffectedRange,
		// identifiers
		a.identifiersString(),
		// fixed_versions
		a.fixedVersionsString(),
		// urls
		// gemnasium uses the `urls` field and represents them as links
		a.linksString(),
		// links
		// there are no links in the yaml files at the moment so we return an empty array
		"{}",
	}
}

// Version contains meta-information for a single version
type Version struct {
	Number string `yaml:"number" json:"number"`
	Commit Commit `yaml:"commit" json:"commit"`
}

// Commit contains meta-information w.r.t. a single git commit
type Commit struct {
	Tags      []string `yaml:"tags" json:"tags"`
	Sha       string   `yaml:"sha" json:"sha"`
	Timestamp string   `yaml:"timestamp" json:"timestamp"`
}

// Package contains the information needed to resolve a package on the server side.
type Package struct {
	Type string `json:"type"`
	Name string `json:"name"`
}

// Slug returns the package slug/path.
func (p Package) Slug() string {
	return p.Type + "/" + p.Name
}

// UnmarshalYAML decodes a package slug.
func (p *Package) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var slug string
	if err := unmarshal(&slug); err != nil {
		return err
	}

	parts := strings.SplitN(slug, "/", 2)
	if len(parts) > 1 {
		p.Name = parts[1]
	}
	p.Type = parts[0]

	return nil
}
